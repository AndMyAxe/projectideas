This could use a better name.

From @artsyhonker@mastodon.art

I also still don't have a thing I can feed 1000 urls and have it search the
content those websites, and only those websites, for a string of text.

I tried YaCy but it wouldn't go and I couldn't even figure out how to ask for
help.

This feels like it *should* be easy, but... it looks like not existing if I
don't want to write a sort of webcrawler-y thing, which I don't given that I
can't code 

The closest I can get is giving the URLs to bloody Google custom Search engine,
but it doesn't always work and I don't know why. And it feels like a kludge,
like it isn't really meant for what I'm doing.

for bonus points one of those little "add to thingy" browser extension buttons
would be grand, but honestly if I have to enter the URLs by writing left-handed
while standing on one foot, I'll cope.