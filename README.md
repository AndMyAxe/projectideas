This repo is here so that when you have an idea for a project that you think
may be worthwhile but don't have time right now to do it you can list it here
and if anyone wants to make it they can.

Complete descriptions are better, but write whatever you can and we will see
what we can do.

In general feel free to flesh out other project ideas that are listed here.
It is much easier to make something when you have a complete description of it
than going from vague ideas, so any work done fleshing out ideas is helpful.