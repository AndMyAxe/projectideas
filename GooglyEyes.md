I would like a browser extension or something similar that uses simple face
detection and adds googly eyes to every face found.

On a more helpful note it could also be used to block out faces for people who
don't like eye contact.